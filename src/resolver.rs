// spftrace – utility for tracing SPF queries
// Copyright © 2022–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use async_trait::async_trait;
use hickory_resolver::{
    config::{ResolverConfig, ResolverOpts},
    system_conf, TokioAsyncResolver,
};
use std::{
    error::Error,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
    sync::atomic::{AtomicBool, Ordering},
    time::Duration,
};
use tokio::time::{self, Instant};
use viaspf::lookup::{Lookup, LookupError, LookupResult, Name};

/// Single-use, timeout Hickory DNS resolver.
///
/// We have disabled viaspf’s `tokio-timeout` feature and implemented timeout
/// per DNS query here, in order to only generate complete, not truncated
/// traces.
pub struct Resolver {
    inner: TokioAsyncResolver,
    override_txt: AtomicBool,
    initial_txt: String,
    deadline: Instant,
}

impl Resolver {
    pub fn new(timeout: Duration, initial_txt: Option<String>) -> Self {
        let (config, opts) = Default::default();

        Self::new_internal(timeout, initial_txt, config, opts)
    }

    pub fn new_with_system_config(
        timeout: Duration,
        initial_txt: Option<String>,
    ) -> Result<Self, Box<dyn Error>> {
        let (config, opts) = system_conf::read_system_conf()?;

        Ok(Self::new_internal(timeout, initial_txt, config, opts))
    }

    fn new_internal(
        timeout: Duration,
        initial_txt: Option<String>,
        config: ResolverConfig,
        mut opts: ResolverOpts,
    ) -> Self {
        opts.timeout = timeout;

        let inner = TokioAsyncResolver::tokio(config, opts);
        let override_txt = AtomicBool::new(initial_txt.is_some());
        let initial_txt = initial_txt.unwrap_or_default();
        let deadline = Instant::now() + timeout;

        Self {
            inner,
            override_txt,
            initial_txt,
            deadline,
        }
    }
}

#[async_trait]
impl Lookup for Resolver {
    async fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        time::timeout_at(self.deadline, self.inner.lookup_a(name))
            .await
            .map_err(|_| LookupError::Timeout)?
    }

    async fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        time::timeout_at(self.deadline, self.inner.lookup_aaaa(name))
            .await
            .map_err(|_| LookupError::Timeout)?
    }

    async fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
        time::timeout_at(self.deadline, self.inner.lookup_mx(name))
            .await
            .map_err(|_| LookupError::Timeout)?
    }

    async fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        if self.override_txt.swap(false, Ordering::Relaxed) {
            return Ok(vec![self.initial_txt.clone()]);
        }

        time::timeout_at(self.deadline, self.inner.lookup_txt(name))
            .await
            .map_err(|_| LookupError::Timeout)?
    }

    async fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        time::timeout_at(self.deadline, self.inner.lookup_ptr(ip))
            .await
            .map_err(|_| LookupError::Timeout)?
    }
}
