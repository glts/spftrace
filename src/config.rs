// spftrace – utility for tracing SPF queries
// Copyright © 2022 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use std::time::Duration;
use viaspf::DomainName;

pub struct Config {
    pub debug: bool,  // not documented
    pub helo_domain: Option<DomainName>,
    pub hostname: Option<String>,
    pub initial_txt: Option<String>,
    pub line_width: usize,
    pub max_lookups: Option<usize>,
    pub max_void_lookups: Option<usize>,
    pub system_resolver: bool,
    pub time: bool,
    pub timeout: Duration,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            debug: false,
            helo_domain: None,
            hostname: None,
            initial_txt: None,
            line_width: 100,
            max_lookups: None,
            max_void_lookups: None,
            system_resolver: false,
            time: false,
            timeout: Duration::from_secs(30),
        }
    }
}
